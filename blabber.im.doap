<?xml version="1.0"?>
<?xml-stylesheet href="../style.xsl" type="text/xsl"?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
<Project xmlns="http://usefulinc.com/ns/doap#"
         xmlns:foaf="http://xmlns.com/foaf/0.1/"
         xmlns:xmpp="https://linkmauve.fr/ns/xmpp-doap#"
         xmlns:schema="https://schema.org/">
    <name>blabber.im</name>

    <created>2015-07-18</created>

    <shortdesc xml:lang="en">Android XMPP Client</shortdesc>

    <description xml:lang="en">blabber.im is a fork of Conversations with some changes, to improve usability.</description>

    <homepage rdf:resource="https://blabber.im/"/>
    <download-page rdf:resource="https://codeberg.org/kriztan/blabber.im/releases"/>
    <bug-database rdf:resource="https://codeberg.org/kriztan/blabber.im/issues"/>
    <!-- See https://github.com/ewilderj/doap/issues/53 -->
    <developer-forum rdf:resource="xmpp:development@room.pix-art.de?join"/>
    <support-forum rdf:resource="xmpp:support@room.pix-art.de?join"/>

    <license rdf:resource="https://codeberg.org/kriztan/blabber.im/src/branch/master/LICENSE"/>

    <!-- See https://github.com/ewilderj/doap/issues/49 -->
    <language>en</language>

    <schema:logo rdf:resource="https://codeberg.org/kriztan/blabber.im/raw/branch/master/art/logo_android.png"/>
    <!--
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/01.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/02.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/03.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/04.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/05.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/06.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/07.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/08.png'/>
    <schema:screenshot rdf:resource='https://raw.githubusercontent.com/iNPUTmice/Conversations/master/fastlane/metadata/android/en-US/images/phoneScreenshots/09.png'/>
    -->

    <programming-language>Java</programming-language>

    <os>Android</os>

    <category rdf:resource="https://linkmauve.fr/ns/xmpp-doap#category-xmpp"/>
    <category rdf:resource="https://linkmauve.fr/ns/xmpp-doap#category-jabber"/>
    <category rdf:resource="https://linkmauve.fr/ns/xmpp-doap#category-client"/>

    <maintainer>
        <foaf:Person>
            <foaf:name>Christian Schneppe</foaf:name>
            <!--
            <foaf:homepage rdf:resource="https://codeberg.org/kriztan/blabber.im"/>
            -->
        </foaf:Person>
    </maintainer>

    <repository>
        <GitRepository>
            <browse rdf:resource="https://codeberg.org/kriztan/blabber.im"/>
            <location rdf:resource="https://codeberg.org/kriztan/blabber.im.git"/>
        </GitRepository>
    </repository> 

    <implements rdf:resource="https://xmpp.org/rfcs/rfc6120.html"/>
    <implements rdf:resource="https://xmpp.org/rfcs/rfc6121.html"/>
    <implements rdf:resource="https://xmpp.org/rfcs/rfc6122.html"/>
    <implements rdf:resource="https://xmpp.org/rfcs/rfc7590.html"/>

    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0027.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.4</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0030.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>2.5rc3</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0045.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.32.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0048.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0049.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.2</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0054.html"/>
            <xmpp:status>partial</xmpp:status>
            <xmpp:version>1.2</xmpp:version>
            <xmpp:note xml:lang='en'>Avatars only</xmpp:note>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0084.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1.3</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0085.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>2.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0092.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0115.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.5.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0153.html"/>
            <xmpp:status>partial</xmpp:status>
            <xmpp:version>1.1</xmpp:version>
            <xmpp:note xml:lang='en'>Read only. Publication via XEP-0398</xmpp:note>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0163.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.2.1</xmpp:version>
            <xmpp:note>Avatar, Nick, OMEMO</xmpp:note>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0166.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1.2</xmpp:version>
            <xmpp:note>File transfer + A/V calls</xmpp:note>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0167.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.2.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0172.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1</xmpp:version>
            <xmpp:note>read only</xmpp:note>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0176.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0184.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.4.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0191.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.3</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0198.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.6</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0199.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>2.0.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0199.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>2.0.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0215.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.7</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0223.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0234.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.19.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0237.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.3</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0245.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0249.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.2</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0260.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.3</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0261.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0280.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.13.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0293.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0294.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0308.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.2.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0313.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.6.3</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0319.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.2</xmpp:version>
            <xmpp:note>opt-out</xmpp:note>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0320.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0333.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.3</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0338.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0339.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0352.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.3.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0353.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.3.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0357.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.4.0</xmpp:version>
            <xmpp:note>Only available in the playstore version with enabled Google Play Services</xmpp:note>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0363.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0368.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.1.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0377.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.2</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0384.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.3.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0391.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.1.2</xmpp:version>
            <xmpp:since>2.5.8</xmpp:since>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0392.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.6.0</xmpp:version>
            <xmpp:since>2.3.1</xmpp:since>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0393.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.1.4</xmpp:version>
            <xmpp:since>1.22.0</xmpp:since>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0396.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.1</xmpp:version>
            <xmpp:since>2.5.8</xmpp:since>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0398.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.2.1</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0410.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>1.0.1</xmpp:version>
            <xmpp:since>2.5.4</xmpp:since>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0411.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.2.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0424.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.3.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>
    <implements>
        <xmpp:SupportedXep>
            <xmpp:xep rdf:resource="https://xmpp.org/extensions/xep-0454.html"/>
            <xmpp:status>complete</xmpp:status>
            <xmpp:version>0.1.0</xmpp:version>
        </xmpp:SupportedXep>
    </implements>

    <release>
        <Version>
            <revision>3.1.4</revision>
            <created>2022-08-09</created>
            <file-release rdf:resource="https://codeberg.org/kriztan/blabber.im/archive/3.1.4.tar.gz"/>
        </Version>
    </release>
</Project>
</rdf:RDF>
